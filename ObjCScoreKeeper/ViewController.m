//
//  ViewController.m
//  ObjCScoreKeeper
//
//  Created by Canyon Duncan on 9/20/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIView* team1view = [UIView new];
    team1view.frame = CGRectZero;
    team1view.backgroundColor = [UIColor whiteColor];
    team1view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:team1view];

    
    UIView* team2view = [UIView new];
    team2view.frame = CGRectZero;
    team2view.backgroundColor = [UIColor whiteColor];
    team2view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:team2view];
    
    UIView* bottomview = [UIView new];
    bottomview.frame = CGRectZero;
    bottomview.backgroundColor = [UIColor grayColor];
    bottomview.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIButton* resetbutton = [UIButton new];
    resetbutton.frame = CGRectZero;
    resetbutton.translatesAutoresizingMaskIntoConstraints = NO;
    resetbutton.backgroundColor = [UIColor redColor];
    [resetbutton setTitle:@"Reset" forState:UIControlStateNormal];
    
    UITextField* team1Text = [UITextField new];
    team1Text.frame = CGRectZero;
    team1Text.translatesAutoresizingMaskIntoConstraints = NO;
    [team1Text setText: @"HI"];
    [team1Text setBackgroundColor: [UIColor whiteColor]];
    team1Text.layer.cornerRadius=8.0f;
    team1Text.textAlignment = NSTextAlignmentCenter;
    
    UILabel* team1Label = [UILabel new];
    team1Label.frame = CGRectZero;
    team1Label.translatesAutoresizingMaskIntoConstraints = NO;
    [team1Label setText: @"0"];
    [team1Label setBackgroundColor:[UIColor brownColor]];
    team1Label.font=[team1Label.font fontWithSize:75];
    team1Label.textAlignment = NSTextAlignmentCenter;
    
    
    
    UIStepper* team1Stepper = [UIStepper new];
    team1Stepper.frame = CGRectZero;
    team1Stepper.translatesAutoresizingMaskIntoConstraints = NO;
    
    [team1Stepper addTarget:self action:@selector(team1StepperValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    //TeAM 2
    UITextField* team2Text = [UITextField new];
    team2Text.frame = CGRectZero;
    team2Text.translatesAutoresizingMaskIntoConstraints = NO;
    [team2Text setText: @"HI"];
    [team2Text setBackgroundColor: [UIColor whiteColor]];
    team2Text.layer.cornerRadius=8.0f;
    team2Text.textAlignment = NSTextAlignmentCenter;
    
    UILabel* team2Label = [UILabel new];
    team2Label.frame = CGRectZero;
    team2Label.translatesAutoresizingMaskIntoConstraints = NO;
    [team2Label setText: @"0"];
    [team2Label setBackgroundColor:[UIColor brownColor]];
    team2Label.font=[team1Label.font fontWithSize:75];
    team2Label.textAlignment = NSTextAlignmentCenter;
    
    UIStepper* team2Stepper = [UIStepper new];
    team2Stepper.frame = CGRectZero;
    team2Stepper.translatesAutoresizingMaskIntoConstraints = NO;
    //end Team2
    
    [team1view addSubview:team1Stepper];
    [team1view addSubview:team1Label];
    [team1view addSubview:team1Text];
    
    [team2view addSubview:team2Stepper];
    [team2view addSubview:team2Label];
    [team2view addSubview:team2Text];
    
    [bottomview addSubview:resetbutton];
    
    [self.view addSubview:bottomview];
    
    NSDictionary* dictionary = NSDictionaryOfVariableBindings(team1view, team2view, bottomview, resetbutton, team1Text, team1Label, team1Stepper, team2Text, team2Label, team2Stepper);
    NSDictionary* metrics = @{};
    
    //BOTTOM VIEW
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"H:|-5-[bottomview]-5-|" options:0 metrics:metrics views:dictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|-[bottomview(50)]-5-|" options:0 metrics:metrics views:dictionary]];
    
    //TEAM 1
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"H:|-5-[team1view]-5-[team2view]-5-|" options:0 metrics:metrics views:dictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|-20-[team1view]-[bottomview]-|" options:0 metrics:metrics views:dictionary]];
    
    
    //equal widths
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"[team1view(==team2view)]" options:0 metrics:metrics views:dictionary]];
    
    //TEAM2
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|-20-[team2view]-[bottomview]-|" options:0 metrics:metrics views:dictionary]];
    
    //reset button
    [bottomview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"H:|-[resetbutton]-|" options:0 metrics:metrics views:dictionary]];
    [bottomview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|-[resetbutton]-|" options:0 metrics:metrics views:dictionary]];
    
    //TEAM1VIEW
    //team1text
    [team1view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"H:|-20-[team1Text]-20-|" options:0 metrics:metrics views:dictionary]];
    [team1view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|-20-[team1Text(40)]-40-[team1Label]-20-[team1Stepper]-10-|" options:0 metrics:metrics views:dictionary]];
    
    //team1Label
    [team1view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[team1Label]-20-|" options:0 metrics:metrics views:dictionary]];
    
    [team1view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[team1Stepper]-|" options:0 metrics:metrics views:dictionary]];
    
    //TEAM 2 VIEW
    
    [team2view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"H:|-20-[team2Text]-20-|" options:0 metrics:metrics views:dictionary]];
    [team2view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|-20-[team2Text(40)]-40-[team2Label]-20-[team2Stepper]-10-|" options:0 metrics:metrics views:dictionary]];

    [team2view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[team2Label]-20-|" options:0 metrics:metrics views:dictionary]];
    
    [team2view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[team2Stepper]-20-|" options:0 metrics:metrics views:dictionary]];
    
    
    
   // [team1view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[team1Text]-40-[team1Label]-40-|" options:NSLayoutFormatAlignAllBottom metrics:metrics views:dictionary]];
}


- (void)team1StepperValueChanged:(UIStepper *)stepper{
    
    NSString *stepperValue = [NSString stringWithFormat:@"%.0f", stepper.value];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
